import React, { Component } from 'react'

export default class Item extends Component {
  render() {
    return (
      <div id='bodyItem' className='row'>
        <div className='col-4 border'>
          <div className='itemIcon'>
              <i class="bi bi-collection-fill"></i>
          </div>
          <div className='itemContent'></div>
        </div>
      </div>
    )
  }
}
