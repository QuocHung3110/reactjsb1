import React, { Component } from "react";
import Banner from "./Body/Banner";
import Item from "./Body/Item";
import "../css/body.css";

export default class Body extends Component {
  render() {
    return (
      <div id="body">
        <Banner></Banner>
        <Item></Item>
      </div>
    );
  }
}
